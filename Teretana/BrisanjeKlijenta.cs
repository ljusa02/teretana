﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Teretana
{

    // Brisanje klijenata

    public partial class BrisanjeKlijenta : Form
    {

        Form1 forma;

        public BrisanjeKlijenta(Form1 forma)
        {
            InitializeComponent();
            popuniComboBox();
            this.forma = forma;
        }

        private void BrisanjeKlijenta_Load(object sender, EventArgs e)
        {

        }

        public void popuniComboBox()
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();


            SqlCommand popuni = new SqlCommand("select IDKlijent, Ime + ' ' + Prezime as Ime from Klijent", conn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = popuni;
            DataTable tabela = new DataTable();
            adapter.Fill(tabela);

            cbKlijent.DataSource = tabela;
            cbKlijent.DisplayMember = "Ime";        // Dodati prezime
            cbKlijent.ValueMember = "IDKlijent";

            conn.Close();

        }

        private void btnIzbrisi_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand obrisi = new SqlCommand("ObrisiKlijenta", conn);
            obrisi.CommandType = CommandType.StoredProcedure;
            obrisi.Parameters.AddWithValue("@idklijent", cbKlijent.SelectedValue);
            obrisi.ExecuteNonQuery();

            conn.Close();

            popuniComboBox();

            MessageBox.Show("Uspesno ste obrisali klijenta.", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

            forma.prikazi();
        }
    }
}
