﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Teretana
{
    public partial class PrikazRadnika : Form
    {
        public PrikazRadnika()
        {
            InitializeComponent();
            popuniDataGrid();
        }

        public void popuniDataGrid()
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand pozivProcedure = new SqlCommand("PrikaziSveRadnike", conn);
            SqlDataAdapter adapter = new SqlDataAdapter(pozivProcedure);
            DataTable tabela = new DataTable();
            adapter.Fill(tabela);
            dataGridView1.DataSource = tabela;

            conn.Close();
        }
    }
}
