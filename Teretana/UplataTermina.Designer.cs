﻿namespace Teretana
{
    partial class UplataTermina
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbKlijent = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbVrstaTermina = new System.Windows.Forms.ComboBox();
            this.nudBrojTermina = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.btnZakazi = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpPocetak = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpKraj = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.nudBrojTermina)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Klijent";
            // 
            // cbKlijent
            // 
            this.cbKlijent.FormattingEnabled = true;
            this.cbKlijent.Location = new System.Drawing.Point(37, 43);
            this.cbKlijent.Name = "cbKlijent";
            this.cbKlijent.Size = new System.Drawing.Size(182, 24);
            this.cbKlijent.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Vrsta termina";
            // 
            // cbVrstaTermina
            // 
            this.cbVrstaTermina.FormattingEnabled = true;
            this.cbVrstaTermina.Location = new System.Drawing.Point(37, 111);
            this.cbVrstaTermina.Name = "cbVrstaTermina";
            this.cbVrstaTermina.Size = new System.Drawing.Size(182, 24);
            this.cbVrstaTermina.TabIndex = 3;
            // 
            // nudBrojTermina
            // 
            this.nudBrojTermina.Location = new System.Drawing.Point(37, 178);
            this.nudBrojTermina.Name = "nudBrojTermina";
            this.nudBrojTermina.Size = new System.Drawing.Size(84, 22);
            this.nudBrojTermina.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 155);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Broj termina";
            // 
            // btnZakazi
            // 
            this.btnZakazi.Location = new System.Drawing.Point(95, 384);
            this.btnZakazi.Name = "btnZakazi";
            this.btnZakazi.Size = new System.Drawing.Size(75, 23);
            this.btnZakazi.TabIndex = 6;
            this.btnZakazi.Text = "Zakazi";
            this.btnZakazi.UseVisualStyleBackColor = true;
            this.btnZakazi.Click += new System.EventHandler(this.btnZakazi_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Datum pocetka";
            // 
            // dtpPocetak
            // 
            this.dtpPocetak.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpPocetak.Location = new System.Drawing.Point(37, 244);
            this.dtpPocetak.Name = "dtpPocetak";
            this.dtpPocetak.Size = new System.Drawing.Size(115, 22);
            this.dtpPocetak.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 293);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Datum kraja";
            // 
            // dtpKraj
            // 
            this.dtpKraj.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpKraj.Location = new System.Drawing.Point(37, 314);
            this.dtpKraj.Name = "dtpKraj";
            this.dtpKraj.Size = new System.Drawing.Size(115, 22);
            this.dtpKraj.TabIndex = 10;
            // 
            // UplataTermina
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(269, 432);
            this.Controls.Add(this.dtpKraj);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dtpPocetak);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnZakazi);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nudBrojTermina);
            this.Controls.Add(this.cbVrstaTermina);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbKlijent);
            this.Controls.Add(this.label1);
            this.Name = "UplataTermina";
            this.Text = "UplataTermina";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UplataTermina_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.nudBrojTermina)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbKlijent;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbVrstaTermina;
        private System.Windows.Forms.NumericUpDown nudBrojTermina;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnZakazi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpPocetak;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpKraj;
    }
}