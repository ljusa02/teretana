﻿namespace Teretana
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.klijentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodavanjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brisanjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clanarineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novaUplataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.radniciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prikazToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodavanjeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.nudGodineOD = new System.Windows.Forms.NumericUpDown();
            this.nudGodineDo = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbIme = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbMesto = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tbPrezime = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.nudVisinaDO = new System.Windows.Forms.NumericUpDown();
            this.nudVisinaOD = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnPrikazi = new System.Windows.Forms.Button();
            this.btnPrikaziSve = new System.Windows.Forms.Button();
            this.lblID = new System.Windows.Forms.Label();
            this.lblDatumPocetka = new System.Windows.Forms.Label();
            this.lblDatumZavrsetka = new System.Windows.Forms.Label();
            this.lblPreostaloTermina = new System.Windows.Forms.Label();
            this.lblVrstaTermina = new System.Windows.Forms.Label();
            this.btnSmanjiTermin = new System.Windows.Forms.Button();
            this.brisanjeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudGodineOD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGodineDo)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudVisinaDO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVisinaOD)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(36, 222);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(878, 244);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.klijentToolStripMenuItem,
            this.clanarineToolStripMenuItem,
            this.radniciToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(962, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // klijentToolStripMenuItem
            // 
            this.klijentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodavanjeToolStripMenuItem,
            this.brisanjeToolStripMenuItem});
            this.klijentToolStripMenuItem.Name = "klijentToolStripMenuItem";
            this.klijentToolStripMenuItem.Size = new System.Drawing.Size(63, 24);
            this.klijentToolStripMenuItem.Text = "Klijent";
            // 
            // dodavanjeToolStripMenuItem
            // 
            this.dodavanjeToolStripMenuItem.Name = "dodavanjeToolStripMenuItem";
            this.dodavanjeToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.dodavanjeToolStripMenuItem.Text = "Dodavanje";
            this.dodavanjeToolStripMenuItem.Click += new System.EventHandler(this.dodavanjeToolStripMenuItem_Click);
            // 
            // brisanjeToolStripMenuItem
            // 
            this.brisanjeToolStripMenuItem.Name = "brisanjeToolStripMenuItem";
            this.brisanjeToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.brisanjeToolStripMenuItem.Text = "Brisanje";
            this.brisanjeToolStripMenuItem.Click += new System.EventHandler(this.brisanjeToolStripMenuItem_Click);
            // 
            // clanarineToolStripMenuItem
            // 
            this.clanarineToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novaUplataToolStripMenuItem});
            this.clanarineToolStripMenuItem.Name = "clanarineToolStripMenuItem";
            this.clanarineToolStripMenuItem.Size = new System.Drawing.Size(83, 24);
            this.clanarineToolStripMenuItem.Text = "Clanarine";
            // 
            // novaUplataToolStripMenuItem
            // 
            this.novaUplataToolStripMenuItem.Name = "novaUplataToolStripMenuItem";
            this.novaUplataToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.novaUplataToolStripMenuItem.Text = "Nova uplata";
            this.novaUplataToolStripMenuItem.Click += new System.EventHandler(this.novaUplataToolStripMenuItem_Click);
            // 
            // radniciToolStripMenuItem
            // 
            this.radniciToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.prikazToolStripMenuItem,
            this.dodavanjeToolStripMenuItem1,
            this.brisanjeToolStripMenuItem1});
            this.radniciToolStripMenuItem.Name = "radniciToolStripMenuItem";
            this.radniciToolStripMenuItem.Size = new System.Drawing.Size(70, 24);
            this.radniciToolStripMenuItem.Text = "Radnici";
            // 
            // prikazToolStripMenuItem
            // 
            this.prikazToolStripMenuItem.Name = "prikazToolStripMenuItem";
            this.prikazToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.prikazToolStripMenuItem.Text = "Prikaz";
            this.prikazToolStripMenuItem.Click += new System.EventHandler(this.prikazToolStripMenuItem_Click);
            // 
            // dodavanjeToolStripMenuItem1
            // 
            this.dodavanjeToolStripMenuItem1.Name = "dodavanjeToolStripMenuItem1";
            this.dodavanjeToolStripMenuItem1.Size = new System.Drawing.Size(181, 26);
            this.dodavanjeToolStripMenuItem1.Text = "Dodavanje";
            this.dodavanjeToolStripMenuItem1.Click += new System.EventHandler(this.dodavanjeToolStripMenuItem1_Click);
            // 
            // nudGodineOD
            // 
            this.nudGodineOD.Location = new System.Drawing.Point(43, 22);
            this.nudGodineOD.Name = "nudGodineOD";
            this.nudGodineOD.Size = new System.Drawing.Size(69, 22);
            this.nudGodineOD.TabIndex = 2;
            this.nudGodineOD.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // nudGodineDo
            // 
            this.nudGodineDo.Location = new System.Drawing.Point(43, 58);
            this.nudGodineDo.Name = "nudGodineDo";
            this.nudGodineDo.Size = new System.Drawing.Size(69, 22);
            this.nudGodineDo.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(435, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Godine";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.nudGodineOD);
            this.panel1.Controls.Add(this.nudGodineDo);
            this.panel1.Location = new System.Drawing.Point(438, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(138, 100);
            this.panel1.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "do";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "od";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Ime i prezime";
            // 
            // tbIme
            // 
            this.tbIme.Location = new System.Drawing.Point(16, 22);
            this.tbIme.Name = "tbIme";
            this.tbIme.Size = new System.Drawing.Size(136, 22);
            this.tbIme.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(241, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Mesto";
            // 
            // tbMesto
            // 
            this.tbMesto.Location = new System.Drawing.Point(16, 21);
            this.tbMesto.Name = "tbMesto";
            this.tbMesto.Size = new System.Drawing.Size(136, 22);
            this.tbMesto.TabIndex = 9;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.tbPrezime);
            this.panel2.Controls.Add(this.tbIme);
            this.panel2.Location = new System.Drawing.Point(52, 66);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(170, 101);
            this.panel2.TabIndex = 10;
            // 
            // tbPrezime
            // 
            this.tbPrezime.Location = new System.Drawing.Point(16, 59);
            this.tbPrezime.Name = "tbPrezime";
            this.tbPrezime.Size = new System.Drawing.Size(136, 22);
            this.tbPrezime.TabIndex = 8;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.tbMesto);
            this.panel3.Location = new System.Drawing.Point(244, 67);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(170, 65);
            this.panel3.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(596, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "Visina";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.nudVisinaDO);
            this.panel4.Controls.Add(this.nudVisinaOD);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Location = new System.Drawing.Point(599, 67);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(140, 100);
            this.panel4.TabIndex = 13;
            // 
            // nudVisinaDO
            // 
            this.nudVisinaDO.Location = new System.Drawing.Point(44, 61);
            this.nudVisinaDO.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.nudVisinaDO.Name = "nudVisinaDO";
            this.nudVisinaDO.Size = new System.Drawing.Size(72, 22);
            this.nudVisinaDO.TabIndex = 3;
            // 
            // nudVisinaOD
            // 
            this.nudVisinaOD.Location = new System.Drawing.Point(44, 20);
            this.nudVisinaOD.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.nudVisinaOD.Name = "nudVisinaOD";
            this.nudVisinaOD.Size = new System.Drawing.Size(72, 22);
            this.nudVisinaOD.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 67);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 17);
            this.label8.TabIndex = 1;
            this.label8.Text = "do";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "od";
            // 
            // btnPrikazi
            // 
            this.btnPrikazi.Location = new System.Drawing.Point(802, 82);
            this.btnPrikazi.Name = "btnPrikazi";
            this.btnPrikazi.Size = new System.Drawing.Size(97, 26);
            this.btnPrikazi.TabIndex = 14;
            this.btnPrikazi.Text = "Pretrazi";
            this.btnPrikazi.UseVisualStyleBackColor = true;
            this.btnPrikazi.Click += new System.EventHandler(this.btnPrikazi_Click);
            // 
            // btnPrikaziSve
            // 
            this.btnPrikaziSve.Location = new System.Drawing.Point(802, 121);
            this.btnPrikaziSve.Name = "btnPrikaziSve";
            this.btnPrikaziSve.Size = new System.Drawing.Size(97, 26);
            this.btnPrikaziSve.TabIndex = 15;
            this.btnPrikaziSve.Text = "Prikazi sve";
            this.btnPrikaziSve.UseVisualStyleBackColor = true;
            this.btnPrikaziSve.Click += new System.EventHandler(this.btnPrikaziSve_Click);
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(46, 192);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(29, 17);
            this.lblID.TabIndex = 16;
            this.lblID.Text = "ID: ";
            // 
            // lblDatumPocetka
            // 
            this.lblDatumPocetka.AutoSize = true;
            this.lblDatumPocetka.Location = new System.Drawing.Point(130, 192);
            this.lblDatumPocetka.Name = "lblDatumPocetka";
            this.lblDatumPocetka.Size = new System.Drawing.Size(111, 17);
            this.lblDatumPocetka.TabIndex = 17;
            this.lblDatumPocetka.Text = "Datum pocetka: ";
            // 
            // lblDatumZavrsetka
            // 
            this.lblDatumZavrsetka.AutoSize = true;
            this.lblDatumZavrsetka.Location = new System.Drawing.Point(337, 192);
            this.lblDatumZavrsetka.Name = "lblDatumZavrsetka";
            this.lblDatumZavrsetka.Size = new System.Drawing.Size(122, 17);
            this.lblDatumZavrsetka.TabIndex = 18;
            this.lblDatumZavrsetka.Text = "Datum zavrsetka: ";
            // 
            // lblPreostaloTermina
            // 
            this.lblPreostaloTermina.AutoSize = true;
            this.lblPreostaloTermina.Location = new System.Drawing.Point(570, 191);
            this.lblPreostaloTermina.Name = "lblPreostaloTermina";
            this.lblPreostaloTermina.Size = new System.Drawing.Size(127, 17);
            this.lblPreostaloTermina.TabIndex = 19;
            this.lblPreostaloTermina.Text = "Preostalo termina: ";
            // 
            // lblVrstaTermina
            // 
            this.lblVrstaTermina.AutoSize = true;
            this.lblVrstaTermina.Location = new System.Drawing.Point(736, 190);
            this.lblVrstaTermina.Name = "lblVrstaTermina";
            this.lblVrstaTermina.Size = new System.Drawing.Size(100, 17);
            this.lblVrstaTermina.TabIndex = 20;
            this.lblVrstaTermina.Text = "Vrsta termina: ";
            // 
            // btnSmanjiTermin
            // 
            this.btnSmanjiTermin.Location = new System.Drawing.Point(438, 497);
            this.btnSmanjiTermin.Name = "btnSmanjiTermin";
            this.btnSmanjiTermin.Size = new System.Drawing.Size(110, 28);
            this.btnSmanjiTermin.TabIndex = 21;
            this.btnSmanjiTermin.Text = "Smanji termin";
            this.btnSmanjiTermin.UseVisualStyleBackColor = true;
            this.btnSmanjiTermin.Click += new System.EventHandler(this.btnSmanjiTermin_Click);
            // 
            // brisanjeToolStripMenuItem1
            // 
            this.brisanjeToolStripMenuItem1.Name = "brisanjeToolStripMenuItem1";
            this.brisanjeToolStripMenuItem1.Size = new System.Drawing.Size(181, 26);
            this.brisanjeToolStripMenuItem1.Text = "Brisanje";
            this.brisanjeToolStripMenuItem1.Click += new System.EventHandler(this.brisanjeToolStripMenuItem1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 552);
            this.Controls.Add(this.btnSmanjiTermin);
            this.Controls.Add(this.lblVrstaTermina);
            this.Controls.Add(this.lblPreostaloTermina);
            this.Controls.Add(this.lblDatumZavrsetka);
            this.Controls.Add(this.lblDatumPocetka);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.btnPrikaziSve);
            this.Controls.Add(this.btnPrikazi);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Teretana";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudGodineOD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGodineDo)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudVisinaDO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVisinaOD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem klijentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodavanjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem brisanjeToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown nudGodineOD;
        private System.Windows.Forms.NumericUpDown nudGodineDo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbIme;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbMesto;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.NumericUpDown nudVisinaDO;
        private System.Windows.Forms.NumericUpDown nudVisinaOD;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnPrikazi;
        private System.Windows.Forms.Button btnPrikaziSve;
        private System.Windows.Forms.ToolStripMenuItem clanarineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novaUplataToolStripMenuItem;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblDatumPocetka;
        private System.Windows.Forms.Label lblDatumZavrsetka;
        private System.Windows.Forms.Label lblPreostaloTermina;
        private System.Windows.Forms.Label lblVrstaTermina;
        private System.Windows.Forms.Button btnSmanjiTermin;
        private System.Windows.Forms.TextBox tbPrezime;
        private System.Windows.Forms.ToolStripMenuItem radniciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prikazToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodavanjeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem brisanjeToolStripMenuItem1;
    }
}

