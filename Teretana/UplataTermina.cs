﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Teretana
{
    public partial class UplataTermina : Form
    {

        Form1 forma;

        public UplataTermina(Form1 forma)
        {
            InitializeComponent();
            popuniComboKlijent();
            popuniComboVrstaTermina();
            this.forma = forma;
        }

        public void popuniComboKlijent()
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand popuniKlijenta = new SqlCommand("select IDKlijent, Ime + ' ' + Prezime as Ime from Klijent", conn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataTable tabela = new DataTable();
            adapter.SelectCommand = popuniKlijenta;
            adapter.Fill(tabela);
            cbKlijent.DataSource = tabela;
            cbKlijent.DisplayMember = "Ime";
            cbKlijent.ValueMember = "IDKlijent";

            conn.Close();
        }

        public void popuniComboVrstaTermina()
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand popuniTermine = new SqlCommand("select IDVrstaTermina, NazivTermina from VrstaTermina", conn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataTable tabela = new DataTable();
            adapter.SelectCommand = popuniTermine;
            adapter.Fill(tabela);
            cbVrstaTermina.DataSource = tabela;
            cbVrstaTermina.DisplayMember = "NazivTermina";
            cbVrstaTermina.ValueMember = "IDVrstaTermina";

            conn.Close();
        }

        private void btnZakazi_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand zakaziTermin = new SqlCommand("insert into KlijentXrefTermin (IDKlijent, IDVrstaTermina, DatumPocetka, DatumZavrsetka, PreostaloTermina) values (@idKlijent, @idVrstaTermina, @datPoc, @datZav, @preostaloTermina)", conn);
            zakaziTermin.Parameters.AddWithValue("@idKlijent", cbKlijent.SelectedValue);
            zakaziTermin.Parameters.AddWithValue("@idVrstaTermina", cbVrstaTermina.SelectedValue);
            zakaziTermin.Parameters.AddWithValue("@datPoc", dtpPocetak.Value);
            zakaziTermin.Parameters.AddWithValue("@datZav", dtpKraj.Value);
            zakaziTermin.Parameters.AddWithValue("@preostaloTermina", nudBrojTermina.Value);

            if (dtpKraj.Value > dtpPocetak.Value)
            {
                zakaziTermin.ExecuteNonQuery();
                MessageBox.Show("Uspesno ste uplatili termin", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else MessageBox.Show("Niste dobro uneli datume", "Greska", MessageBoxButtons.OK, MessageBoxIcon.Error);

            conn.Close();

            forma.prikazi();
        }

        private void UplataTermina_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
    }
}
