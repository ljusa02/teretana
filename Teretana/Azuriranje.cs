﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Teretana
{
    public partial class Azuriranje : Form
    {

        public int ID;

        Form1 forma;

        public Azuriranje(int ID, Form1 forma)
        {
            InitializeComponent();
            this.ID = ID;
            popuniComboBox();
            ucitajPodatke();
            this.forma = forma;
        }

        public void ucitajPodatke()
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand komanda = new SqlCommand("select k.Ime, k.Prezime, k.DatumRodjenja, k.Visina, k.Tezina, k.IDMesto from Klijent as k where k.IDKlijent = @ID", conn);
            komanda.Parameters.AddWithValue("@ID", ID);

            SqlDataReader ucitaj = komanda.ExecuteReader();

            while(ucitaj.Read())
            {
                tbIme.Text = ucitaj.GetString(0);
                tbPrezime.Text = ucitaj.GetString(1);
                dateTimePicker1.Value = ucitaj.GetDateTime(2);
                nudVisina.Value = ucitaj.GetInt32(3);
                nudTezina.Value = ucitaj.GetInt32(4);
                cbMesto.SelectedValue = ucitaj.GetInt32(5);
            }

            conn.Close();
        }

        public void popuniComboBox()
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand komanda = new SqlCommand("select IDMesto, NazivMesta from Mesto", conn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataTable tabela = new DataTable();
            adapter.SelectCommand = komanda;
            adapter.Fill(tabela);

            cbMesto.DataSource = tabela;
            cbMesto.DisplayMember = "NazivMesta";
            cbMesto.ValueMember = "IDMesto";

            conn.Close();
        }

        private void btnAzuriraj_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand izmeni = new SqlCommand("update Klijent set Ime = @ime, Prezime = @prezime, DatumRodjenja = @datrodj, Visina = @visina, Tezina = @tezina, IDMesto = @idmesto where IDKlijent = @idklijent", conn);
            izmeni.Parameters.AddWithValue("@ime", tbIme.Text);
            izmeni.Parameters.AddWithValue("@prezime", tbPrezime.Text);
            izmeni.Parameters.AddWithValue("@datrodj", dateTimePicker1.Value);
            izmeni.Parameters.AddWithValue("@visina", nudVisina.Value);
            izmeni.Parameters.AddWithValue("@tezina", nudTezina.Value);
            izmeni.Parameters.AddWithValue("@idmesto", cbMesto.SelectedValue);
            izmeni.Parameters.AddWithValue("@idklijent", ID);
            izmeni.ExecuteNonQuery();

            MessageBox.Show("Uspesno ste azurirali klijenta");

            conn.Close();

            forma.prikazi();
        }
    }
}
