﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Teretana
{
    public partial class Form1 : Form
    {

        List<KlijentTermin> lista = null;
        int IDXref;

        public Form1()
        {
            InitializeComponent();
            popuniDataGridSVE();
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.Rows[0].Selected = true;
            dataGridView1.Columns[1].ReadOnly = true;
            dataGridView1.Columns[2].ReadOnly = true;
            dataGridView1.Columns[3].ReadOnly = true;
            dataGridView1.Columns[4].ReadOnly = true;
            dataGridView1.Columns[5].ReadOnly = true;
            dataGridView1.Columns[6].ReadOnly = true;
        }

        private void dodavanjeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DodajKlijenta dk = new DodajKlijenta(this);
            dk.Show();
        }

        private void brisanjeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BrisanjeKlijenta bk = new BrisanjeKlijenta(this);
            bk.Show();
        }

        private void novaUplataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UplataTermina ut = new UplataTermina(this);
            ut.Show();
        }

        public void popuniListu()
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();


            lista = new List<KlijentTermin>();
            SqlCommand popuni = new SqlCommand("TerminiKlijenti", conn);
            popuni.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = popuni.ExecuteReader();

            while (dr.Read())
            {

                int ID = dr.GetInt32(0);
                DateTime pocetak = dr.GetDateTime(1);
                DateTime kraj = dr.GetDateTime(2);
                int ostatakTermina = dr.GetInt32(3);
                string naziv = dr.GetString(4);
                int idXref = dr.GetInt32(5);

                KlijentTermin kt = new KlijentTermin(ID, pocetak, kraj, ostatakTermina, naziv, idXref);


                lista.Add(kt);
            }

            conn.Close();
        }

        public void popuniDataGridSVE()
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand popuni = new SqlCommand("select IDKlijent, Ime, Prezime, DatumRodjenja, Visina, Tezina, m.NazivMesta from Klijent as k inner join Mesto as m on m.IDMesto = k.IDMesto", conn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataTable tabela = new DataTable();
            adapter.SelectCommand = popuni;
            adapter.Fill(tabela);
            dataGridView1.DataSource = tabela;
            dataGridView1.Columns["IDKlijent"].Visible = false;

            conn.Close();

            popuniListu();
        }

        private void btnPrikaziSve_Click(object sender, EventArgs e)
        {
            prikazi();
        }

        public void prikazi()
        {
            popuniDataGridSVE();
            popuniListu();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow dr = dataGridView1.SelectedRows[0];
                lblID.Text = "ID: " + dr.Cells[0].Value.ToString();
                IDXref = pronadjiKlijenta(Convert.ToInt32(dr.Cells[0].Value));

                if (IDXref == 0)
                    btnSmanjiTermin.Enabled = false;
                else btnSmanjiTermin.Enabled = true;
            }
            catch (Exception ex)
            {

            }
        }

        public int pronadjiKlijenta(int ID)
        {
            int flag = 0;
            int id = 0;

            for (int i = 0; i < lista.Count; i++)
                if (lista.ElementAt(i).IDKlijent1 == ID)
                {
                    lblDatumPocetka.Text = "Datum pocetka: " + lista.ElementAt(i).DatPocetka.ToString("dd/MM/yyyy");
                    lblDatumZavrsetka.Text = "Datum zavrsetka: " + lista.ElementAt(i).DatZavrsetka.ToString("dd/MM/yyyy");
                    lblPreostaloTermina.Text = "Preostalo termina: " + lista.ElementAt(i).PreostaloTermina;
                    lblVrstaTermina.Text = "Vrsta termina: " + lista.ElementAt(i).NazivTermina1;

                    id = lista.ElementAt(i).IDXref1;

                    flag = 1;
                }


            if (flag == 0)
            {
                lblDatumPocetka.Text = "Datum pocetka: ";
                lblDatumZavrsetka.Text = "Datum zavrsetka: ";
                lblPreostaloTermina.Text = "Preostalo termina: ";
                lblVrstaTermina.Text = "Vrsta termina: ";
            }

            return id;
        }

        private void btnSmanjiTermin_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand smanjiTermin = new SqlCommand("update KlijentXrefTermin set PreostaloTermina = PreostaloTermina - 1 where IDKlijentXref = @id", conn);
            smanjiTermin.Parameters.AddWithValue("@id", IDXref);
            smanjiTermin.ExecuteNonQuery();

            conn.Close();

            popuniDataGridSVE();
            popuniListu();

            MessageBox.Show("Uspesno ste samnjili broj termina");
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btnPrikazi_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand pozivProcedure = new SqlCommand("PretragaKlijenata", conn);
            pozivProcedure.CommandType = CommandType.StoredProcedure;

            if (string.IsNullOrEmpty(tbIme.Text))
            {
                pozivProcedure.Parameters.AddWithValue("@ime", DBNull.Value);
            }
            else pozivProcedure.Parameters.AddWithValue("@ime", tbIme.Text);

            if (string.IsNullOrEmpty(tbPrezime.Text))
            {
                pozivProcedure.Parameters.AddWithValue("@prezime", DBNull.Value);
            }
            else pozivProcedure.Parameters.AddWithValue("@prezime", tbPrezime.Text);

            if (string.IsNullOrEmpty(tbMesto.Text))
            {
                pozivProcedure.Parameters.AddWithValue("@mesto", DBNull.Value);
            }
            else pozivProcedure.Parameters.AddWithValue("@mesto", tbMesto.Text);

            if (nudGodineOD.Value == 0 && nudGodineDo.Value == 0)
            {
                pozivProcedure.Parameters.AddWithValue("@godineOD", DBNull.Value);
                pozivProcedure.Parameters.AddWithValue("@godineDO", DBNull.Value);
            }
            else
            {
                pozivProcedure.Parameters.AddWithValue("@godineOD", nudGodineOD.Value);
                pozivProcedure.Parameters.AddWithValue("@godineDO", nudGodineDo.Value);
            }

            if (nudVisinaOD.Value == 0 && nudVisinaDO.Value == 0)
            {
                pozivProcedure.Parameters.AddWithValue("@visinaOD", DBNull.Value);
                pozivProcedure.Parameters.AddWithValue("@visinaDO", DBNull.Value);
            }

            else
            {
                pozivProcedure.Parameters.AddWithValue("@visinaOD", nudVisinaOD.Value);
                pozivProcedure.Parameters.AddWithValue("@visinaDO", nudVisinaDO.Value);
            }
        
            SqlDataAdapter adapt = new SqlDataAdapter(pozivProcedure);
            DataTable tabela = new DataTable();
            adapt.Fill(tabela);

            dataGridView1.DataSource = tabela;

            conn.Close();

        }

        private void prikazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrikazRadnika pr = new PrikazRadnika();
            pr.Show();
        }

        private void dodavanjeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DodajRadnika dr = new DodajRadnika();
            dr.Show();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow dr = dataGridView1.SelectedRows[0];
            int id = Convert.ToInt32(dr.Cells[0].Value);

            Azuriranje a = new Azuriranje(id, this);
            a.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void brisanjeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            BrisanjeRadnika br = new BrisanjeRadnika();
            br.Show();
        }
    }
}
