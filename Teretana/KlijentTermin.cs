﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teretana
{
    class KlijentTermin
    {
        int IDKlijent;
        string NazivTermina;
        DateTime datPocetka, datZavrsetka;
        int preostaloTermina;
        int IDXref;

        public KlijentTermin (int idklijent, DateTime datpocetka, DateTime datzavrsetka, int preostalotermina, string nazivtermina, int IDXREF)
        {
            IDKlijent = idklijent;
            NazivTermina = nazivtermina;
            datPocetka = datpocetka;
            DatZavrsetka = datzavrsetka;
            PreostaloTermina = preostalotermina;
            IDXref = IDXREF;
        }

        public int IDKlijent1 { get => IDKlijent; set => IDKlijent = value; }
        public string NazivTermina1 { get => NazivTermina; set => NazivTermina = value; }
        public DateTime DatPocetka { get => datPocetka; set => datPocetka = value; }
        public DateTime DatZavrsetka { get => datZavrsetka; set => datZavrsetka = value; }
        public int PreostaloTermina { get => preostaloTermina; set => preostaloTermina = value; }
        public int IDXref1 { get => IDXref; set => IDXref = value; }
    }
}
