﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Teretana
{
    public partial class DodajKlijenta : Form
    {

        Form1 forma;

        public DodajKlijenta(Form1 forma)
        {
            InitializeComponent();
            popuniComboBox();
            this.forma = forma;
        }

        private void DodajKlijenta_Load(object sender, EventArgs e)
        {

        }

        public void popuniComboBox()
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand komanda = new SqlCommand("select IDMesto, NazivMesta from Mesto", conn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataTable tabela = new DataTable();
            adapter.SelectCommand = komanda;
            adapter.Fill(tabela);

            cbMesto.DataSource = tabela;
            cbMesto.DisplayMember = "NazivMesta";
            cbMesto.ValueMember = "IDMesto";

            conn.Close();
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand dodavanje = new SqlCommand("insert into Klijent (Ime, Prezime, DatumRodjenja, Visina, Tezina, IDMesto) values (@ime, @prezime, @rodjendan, @visina, @tezina, @idmesto)", conn);
            dodavanje.Parameters.AddWithValue("@ime", tbIme.Text);
            dodavanje.Parameters.AddWithValue("@prezime", tbPrezime.Text);
            dodavanje.Parameters.AddWithValue("@rodjendan", dateTimePicker1.Value);
            dodavanje.Parameters.AddWithValue("@visina", nudVisina.Value);
            dodavanje.Parameters.AddWithValue("@tezina", nudTezina.Value);
            dodavanje.Parameters.AddWithValue("@idmesto", cbMesto.SelectedValue);
            dodavanje.ExecuteNonQuery();

            conn.Close();

            tbIme.Clear();
            tbPrezime.Clear();
            nudVisina.Value = 0;
            nudTezina.Value = 0;

            MessageBox.Show("Uspesno ste dodali novog klijenta.", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            forma.prikazi();
        }
    }
}
