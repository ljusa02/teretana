﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Teretana
{
    public partial class BrisanjeRadnika : Form
    {
        public BrisanjeRadnika()
        {
            InitializeComponent();
            popuniComboBox();
        }

        private void BrisanjeRadnika_Load(object sender, EventArgs e)
        {

        }

        public void popuniComboBox()
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand komanda = new SqlCommand("select o.IDOsoblje, o.Ime + ' ' + o.Prezime as ImePrezime from Osoblje as o", conn);
            SqlDataAdapter adapt = new SqlDataAdapter(komanda);
            DataTable tabela = new DataTable();
            adapt.Fill(tabela);

            cbRadnici.DataSource = tabela;
            cbRadnici.DisplayMember = "ImePrezime";
            cbRadnici.ValueMember = "IDOsoblje";

            conn.Close();

        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand komanda = new SqlCommand("delete from Osoblje where IDOsoblje = @id", conn);
            komanda.Parameters.AddWithValue("@id", cbRadnici.SelectedValue);
            komanda.ExecuteNonQuery();

            MessageBox.Show("Uspesno ste izbrisali radnika.");

            conn.Close();

            popuniComboBox();
        }
    }
}
