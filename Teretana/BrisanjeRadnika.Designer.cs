﻿namespace Teretana
{
    partial class BrisanjeRadnika
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbRadnici = new System.Windows.Forms.ComboBox();
            this.btnObrisi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbRadnici
            // 
            this.cbRadnici.FormattingEnabled = true;
            this.cbRadnici.Location = new System.Drawing.Point(47, 63);
            this.cbRadnici.Name = "cbRadnici";
            this.cbRadnici.Size = new System.Drawing.Size(238, 24);
            this.cbRadnici.TabIndex = 0;
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(341, 62);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(102, 24);
            this.btnObrisi.TabIndex = 1;
            this.btnObrisi.Text = "Obrisi";
            this.btnObrisi.UseVisualStyleBackColor = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // BrisanjeRadnika
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 148);
            this.Controls.Add(this.btnObrisi);
            this.Controls.Add(this.cbRadnici);
            this.Name = "BrisanjeRadnika";
            this.Text = "BrisanjeRadnika";
            this.Load += new System.EventHandler(this.BrisanjeRadnika_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbRadnici;
        private System.Windows.Forms.Button btnObrisi;
    }
}