﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Teretana
{
    public partial class DodajRadnika : Form
    {
        public DodajRadnika()
        {
            InitializeComponent();
            popuniComboBoxove();
        }

        public void popuniComboBoxove()
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand popuniMesto = new SqlCommand("select m.IDMesto, m.NazivMesta from Mesto as m", conn);
            SqlDataAdapter adapt1 = new SqlDataAdapter(popuniMesto);
            DataTable t1 = new DataTable();
            adapt1.Fill(t1);
            cbMesto.DataSource = t1;
            cbMesto.ValueMember = "IDMesto";
            cbMesto.DisplayMember = "NazivMesta";

            SqlCommand popuniRadnoMesto = new SqlCommand("select rm.IDRadnoMesto, rm.NazivRadnogMesta from RadnoMesto as rm", conn);
            SqlDataAdapter adapt2 = new SqlDataAdapter(popuniRadnoMesto);
            DataTable t2 = new DataTable();
            adapt2.Fill(t2);
            cbRadnoMesto.DataSource = t2;
            cbRadnoMesto.ValueMember = "IDRadnoMesto";
            cbRadnoMesto.DisplayMember = "NazivRadnogMesta";

            SqlCommand popuniStatus = new SqlCommand("select s.IDStatusRadnika, s.NazivStatusa from StatusRadnika as s", conn);
            SqlDataAdapter adapt3 = new SqlDataAdapter(popuniStatus);
            DataTable t3 = new DataTable();
            adapt3.Fill(t3);
            cbStatusRadnika.DataSource = t3;
            cbStatusRadnika.ValueMember = "IDStatusRadnika";
            cbStatusRadnika.DisplayMember = "NazivStatusa";

            conn.Close();
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(Konekcija.kon);
            conn.Open();

            SqlCommand dodajRadnika = new SqlCommand("DodajRadnika", conn);
            dodajRadnika.CommandType = CommandType.StoredProcedure;
            dodajRadnika.Parameters.AddWithValue("@ime", tbIme.Text);
            dodajRadnika.Parameters.AddWithValue("@prezime", tbPrezime.Text);
            dodajRadnika.Parameters.AddWithValue("@mesto", (int)cbMesto.SelectedValue);
            dodajRadnika.Parameters.AddWithValue("@radnomesto", (int)cbRadnoMesto.SelectedValue);
            dodajRadnika.Parameters.AddWithValue("@statusradnika", (int)cbStatusRadnika.SelectedValue);
            dodajRadnika.Parameters.AddWithValue("@licenca", tbLicenca.Text);
            dodajRadnika.ExecuteNonQuery();

            MessageBox.Show("Uspesno ste dodali radnika");

            conn.Close();

        }
    }
}
